## Synopsis

This project is sample project for Avenue Code Interview Process


## Dependencies


This application depends heavily on the [Spring Boot](http://projects.spring.io/spring-boot/), [Jersey](https://jersey.java.net/) and [H2](http://www.h2database.com/html/main.html)


## Running

Assuming Java and mvn are installed, to run the application, on the root folder, simply run:

```
../root$ mvn spring-boot:run
```

The application should start at port 8080 (note that only one instance of the server can run at time).


## Tests

To run all the tests, on the root folder, run:

```
../root$ mvn test
```

For the sake of simplicity, tests for simple getters and setters are not included.


## Input

Input should be formatted as described in the problem description and provided as a text (.txt) file to be upload to the application.


## License

Apache 2.0, you know the drill ;)
